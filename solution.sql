--a:
SELECT * 
FROM artists 
WHERE name LIKE "%d%";

--b:
SELECT * 
FROM songs 
WHERE length < 230;

--c:
SELECT album_title, song_name, length 
FROM albums
    JOIN songs ON albums.id = songs.album_id;

--d:
SELECT * 
FROM albums 
WHERE album_title LIKE "%a%";

SELECT * 
FROM artists
    JOIN albums on artists.id = albums.artist_id;

--e:
SELECT * 
FROM albums 
ORDER BY album_title 
DESC LIMIT 4;

--f:
SELECT * 
FROM albums
    JOIN songs ON albums.id = songs.album_id;

SELECT * 
FROM albums 
ORDER BY album_title DESC;

SELECT * 
FROM songs 
ORDER BY song_name ASC;